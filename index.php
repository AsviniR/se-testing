<?php
session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    echo "Welcome to the dashboard, " . $_SESSION['username'];
} else {
    header('Location: login.php');
}
?>
