<?php

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;

class TestLogin extends PHPUnit_Framework_TestCase {

    protected $url = 'http://localhost/selenium-php-demo/login.php';
    /**
     * @var RemoteWebDriver
     */
    protected $webDriver;

    public function setUp() {
        $host = 'http://localhost:4444/wd/hub'; // this is the default WebDriver URL
        $this->webDriver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
    }

    public function tearDown() {
        $this->webDriver->quit();
    }

    public function testLogin() {
        $this->webDriver->get($this->url);

        $usernameInput = $this->webDriver->findElement(WebDriverBy::id('username'));
        $passwordInput = $this->webDriver->findElement(WebDriverBy::id('password'));
        $loginButton = $this->webDriver->findElement(WebDriverBy::id('loginButton'));

        $usernameInput->sendKeys('admin');
        $passwordInput->sendKeys('password123');
        $loginButton->
?>
