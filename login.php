<?php
session_start();

$message = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if ($username == "admin" && $password == "password123") {
        $_SESSION['loggedin'] = true;
        $_SESSION['username'] = $username;
        header('Location: index.php');
    } else {
        $message = "Invalid credentials!";
    }
}
?>
<html>
<head><title>Login</title></head>
<body>
<?php if($message) echo "<p>$message</p>"; ?>
<form action="login.php" method="post">
    <label>Username: <input type="text" name="username" id="username"/></label><br/>
    <label>Password: <input type="password" name="password" id="password"/></label><br/>
    <input type="submit" value="Login" id="loginButton"/>
</form>
</body>
</html>
